package main

import "fmt"

type Value struct {
	next  *Value
	pre   *Value
	value interface{}
}
type LinkList struct {
	size int
	root *Value
}

func NewLinkList() *LinkList  {
	return &LinkList{size:0,root:nil}
}

func (l *LinkList)Append(v interface{}) *LinkList {
	var value = &Value{value:v}
	var curValue *Value = l.root
	for {
		if curValue == nil{
			value.pre = nil
			value.next = nil
			l.root = value
			l.size++
			break
		}
		if curValue.next != nil{
			curValue = curValue.next
		}else{
			value.next = nil
			value.pre = curValue
			curValue.next = value
			l.size++
			break
		}
	}
	return l
}

func (l *LinkList)Remove(v interface{}) *Value {

	var curValue *Value = l.root
	for{
		if curValue == nil{
			fmt.Println("no root node,remove error")
			return nil
		}
		if curValue.value == v{
			if curValue.pre == nil{
				l.root = curValue.next
			}else{
				curValue.pre.next = curValue.next
				curValue = nil
				l.size--
				fmt.Println("remove success")
				break
			}
		}
		if curValue.next == nil{
			fmt.Println("no find node,remove error")
			return nil
		}else{
			curValue = curValue.next
		}
	}
	return curValue
}

func (l *LinkList)Size() int {
	return l.size
}

func (l *LinkList)contains(v interface{}) (bool,int){

	var curValue *Value = l.root
	if curValue == nil{
		return false,-1
	}
	var index int = 0
	for{
		if curValue.value == v{
			return true,index
		}
		if curValue.next == nil{
			return false,-1
		}else{
			curValue = curValue.next
			index++
		}
	}
}
func (l *LinkList) toString()  {
	fmt.Println(l)
	var curValue *Value = l.root

	for {
		if curValue == nil{
			break
		}
		fmt.Print(curValue,",")
		curValue = curValue.next
	}
}

func main(){
	list := NewLinkList()
	list.Append("Hello")
	list.Append("WO")
	list.Append("DEL")
	list.Append("FIL")
	list.Append("XXX")
	list.Remove("XXX")
	list.Append("FHJKHD")
	b,i :=  list.contains("FIL")
	if b{
		fmt.Println("FIL is contain,Index is :",i)
	}else{
		fmt.Println("FIL not contain,Index is :",i)
	}
	bb,ii :=  list.contains("XXX")
	if bb{
		fmt.Println("XXX is contain,Index is :",ii)
	}else{
		fmt.Println("XXX not contain,Index is :",ii)
	}
	list.toString()

}

