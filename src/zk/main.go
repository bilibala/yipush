package main


import (
	"fmt"
	zk "github.com/samuel/go-zookeeper/zk"
	"time"
)

/**
 * 获取一个zk连接
 * @return {[type]}
 */
func getConnect(zkList []string) (conn *zk.Conn) {

	conn, _, err := zk.Connect(zkList, 10*time.Second)
	if err != nil {
		fmt.Println(err)
	}
	return
}

/**
 * 测试连接
 * @return
 */
func test1() {
	zkList := []string{"localhost:2181"}
	conn := getConnect(zkList)

	defer conn.Close()
	bytes := []byte("hello zookeeper")
	conn.Create("/go_servers",bytes, 0, zk.WorldACL(zk.PermAll))
}

/**
 * 测试临时节点
 * @return {[type]}
 */
func test2() {
	zkList := []string{"localhost:2181"}
	conn := getConnect(zkList)

	defer conn.Close()
	conn.Create("/testadaadsasdsaw", nil, zk.FlagEphemeral, zk.WorldACL(zk.PermAll))

}

/**
 * 获取所有节点
 */
func test3() {
	zkList := []string{"localhost:2181"}
	conn := getConnect(zkList)

	defer conn.Close()

	data, _, err := conn.Get("/go_servers")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(data))
}

/**
 * 测试输入数据
 * @return
 */
func test4() {
	zkList := []string{"localhost:2181"}
	conn := getConnect(zkList)

	defer conn.Close()
	bytes := []byte("hello")
	stat,err := conn.Set("/fuck6_servers",bytes, 0)
	if err != nil{
		fmt.Println("error =>",err)
	}else{
		fmt.Println("stat =>",stat)
	}
}

func main() {
	test4()
	//test2()
	//test3()
}
