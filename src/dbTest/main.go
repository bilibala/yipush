package main

//import "database/sql"
//import _ "github.com/go-sql-driver/mysql"
import (
	"fmt"
	//"database/sql"
	"time"
	"sync"
	//"sync/atomic"
	//"sync/atomic"
)

//func main(){
//	fmt.Println("db connecting")
//	db, err := sql.Open("mysql", "root:123456@/misaki")
//	if err != nil{
//		fmt.Println("db connect Error :",err)
//	}else{
//		fmt.Println("db connect Success",db)
//	}
//	rows,err := db.Query("SELECT username,id,name FROM pictruewebapp_user")
//	if err != nil{
//		fmt.Println("select user fail :",err)
//	}
//	for rows.Next(){
//		var username string
//		var id int
//		var name string
//		rows.Columns()
//		rows.Scan(&username,&id,&name)
//		fmt.Println(id,"-",username,"-",name)
//	}
//
//}
var p int32 = 0
var lock sync.RWMutex
var pool sync.Pool
func work() {
	var bytes []byte
	p := pool.Get()
	if p == nil{
		p = make([]byte,10)
	}
	bytes = p.([]byte)
	fmt.Print(string(bytes))
	pool.Put(bytes)
	//atomic.AddInt32(&p,1)
	//time.Sleep(time.Millisecond*100)
}
func workNoCache() {
	var bytes []byte
	bytes = make([]byte,10)
	fmt.Print(string(bytes))
	//pool.Put(bytes)
	//atomic.AddInt32(&p,1)
	//time.Sleep(time.Millisecond*100)
}
func worker(id int, jobs chan int,wg *sync.WaitGroup) {

	for {
		j:= <- jobs
		j++
		workNoCache()
		wg.Done()
	}

}

func main() {
	var wg sync.WaitGroup
	var goNum int = 10000
	var goJobNum int = 100000

	jobs := make(chan int, goJobNum)


	for j := 1; j <= goJobNum; j++ {
		wg.Add(1)
		jobs <- j
	}

	for w := 0; w <= goNum; w++ {
		go worker(w, jobs, &wg)
	}

	begin := time.Now()
	wg.Wait()
	cost := time.Since(begin)

	fmt.Println("cost :",cost," p :",p)

}

