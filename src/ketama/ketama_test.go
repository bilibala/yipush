package ketama

import (
	"fmt"
	"strconv"
	"testing"
)

func TestGetInfo(t *testing.T) {
	ring := NewRing(255)

	nodes := map[string]int{
		"test1.server.com": 1,
		"test2.server.com": 1,
		"test3.server.com": 2,
		"test4.server.com": 2,
	}

	for k, v := range nodes {
		ring.AddNode(k, v)
	}

	ring.Bake()

	m := make(map[string]int)
	for i := 0; i < 1e6; i++ {
		m[ring.Hash("test value"+strconv.FormatUint(uint64(i), 10))]++
	}

	for k := range nodes {
		fmt.Println(k, m[k])
	}
	//fmt.Println(ring.Hash("ahsjkahaskjhas"))
	//fmt.Println(ring.Hash("saaadd"))
	//fmt.Println(ring.Hash("sasadghytr"))
	//fmt.Println(ring.Hash("dzcxvxfxf"))
	//fmt.Println(ring.Hash("ahsjkahaskjhas"))
	//fmt.Println(ring.Hash("rwkhxkr"))
	//fmt.Println(ring.Hash("yiunjkxhks"))
	//fmt.Println(ring.Hash("ehiuxhuxir"))
	//fmt.Println(ring.Hash("ewihiunjkxhr"))
	//fmt.Println(ring.Hash("heixhudhue"))
	//fmt.Println(ring.Hash("qguxbcjkhkj"))
	//fmt.Println(ring.Hash("wrhkwhjkehk"))
	//fmt.Println(ring.Hash("hkjxhjkhk"))


}