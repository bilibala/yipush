package main

import (
	"fmt"
	"time"
)
type Msg struct {
	key string
}
func put(m *Msg){
	msg <- m
}
func get(){
	for {
		select {
		case m := <-msg:
			fmt.Print("line :", m)
			fmt.Print("line :", m)
			fmt.Print("line :", m)
			fmt.Println("line :", m)
			fmt.Println("line :", m)
		}
	}
}
var msg chan *Msg
func main() {

	msg = make(chan *Msg,10)
	for i:= 0;i < 10;i++{
		m := &Msg{key:fmt.Sprintf("user %d",i)}
		put(m)
	}
	go get()
	for {
		time.Sleep(10*time.Second)
	}
}
