package main

import (
	"fmt"
	"errors"
)


/*
	TO DO : something change,but i don't know how
	This is a shit Linked List
	Thread insecurity ,you need use the fuck Lock
*/
type Value struct {
	pre   *Value
	value interface{}
	next  *Value

}
type LinkList struct {
	size int
	root *Value
}

func NewLinkList() *LinkList  {
	return &LinkList{size:0,root:nil}
}

func (l *LinkList)Append(v interface{}) *LinkList {
	var value = &Value{value:v}
	var curValue *Value = l.root
	for {
		if curValue == nil{
			value.pre = nil
			value.next = nil
			l.root = value
			l.size++
			break
		}
		if curValue.next != nil{
			curValue = curValue.next
		}else{
			value.next = nil
			value.pre = curValue
			curValue.next = value
			l.size++
			break
		}
	}
	return l
}

func (l *LinkList)Remove(v interface{}) error {

	var curValue *Value = l.root
	err := errors.New("no exist value")
	for{
		if curValue == nil{
			return err
		}
		if curValue.value == v{
			if curValue.pre == nil{
				l.root = curValue.next
			}else{
				curValue.pre.next = curValue.next
				curValue = nil
				l.size--
				break
			}
		}
		if curValue.next == nil{
			return err
		}else{
			curValue = curValue.next
		}
	}
	return nil
}

func (l *LinkList)Size() int {
	return l.size
}

func (l *LinkList)contains(v interface{}) (bool,int){

	var curValue *Value = l.root
	if curValue == nil{
		return false,-1
	}
	var index int = 0
	for{
		if curValue.value == v{
			return true,index
		}
		if curValue.next == nil{
			return false,-1
		}else{
			curValue = curValue.next
			index++
		}
	}
}
func (l *LinkList) toString()  {
	fmt.Println(l)
	var curValue *Value = l.root

	for {
		if curValue == nil{
			break
		}
		fmt.Print(curValue.value,",")
		curValue = curValue.next
	}
}
