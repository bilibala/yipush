package main


const (

	AUTH = int8(1)
	AUTH_REPLY = int8(2)

	HEARTBEAT = int8(3)
	HEARTBEAT_REPLY = int8(4)

	MSG = int8(5)
	MSG_REPLY = int8(6)

	ROOM_MSG = int8(7)
	ROOM_MSG_REPLY = int8(8)

	JOIN_ROOM = int8(9)
	JOIN_ROOM_REPLY = int8(10)

	LEAVE_ROOM = int8(11)
	LEAVE_ROOM_REPLY = int8(11)

)


