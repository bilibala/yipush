package main

import (
	"net/rpc"
	"net"
	"fmt"
)

type DemoRPC struct {
	name string
}
func NewDemoRPC(name string) *DemoRPC{
	return &DemoRPC{name}
}
func (d *DemoRPC) GetName(args *interface{},reply *string) error{
	fmt.Println("rpc call getName():",d.name)
	*reply = d.name
	return nil
}
func (d *DemoRPC) SetName(args *string,reply *string) error{
	fmt.Println("rpc call setName():",*args)
	d.name = *args + " RPC"
	*reply = d.name
	return nil
}

func main() {
	rpc.Register(NewDemoRPC("HelloRPC"))
	l,err := net.Listen("tcp","127.0.0.1:2100")
	defer func(err error) {
		fmt.Println("panic :",err)
		panic(err)
	}(err)
	if err != nil{
		fmt.Println("net.Listen() fail :",err)
		l.Close()
	}
	rpc.Accept(l)

}
