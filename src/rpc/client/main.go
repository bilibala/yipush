package client

import (
	"net/rpc"
	"fmt"
)

func InitRPC() *rpc.Client{
	c,err := rpc.Dial("tcp","127.0.0.1:2100")
	if err != nil{
		fmt.Println("rpc.Dial() fail :",err)
	}
	return c
}
func SetName(c *rpc.Client,name string){
	var reply string
	var args string = name
	if err := c.Call("DemoRPC.SetName",&args,&reply);err != nil{
		fmt.Println("c.Call() fail :",err)
	}
	fmt.Println("reply :",reply)

}
func GetName(c *rpc.Client) (string,error){
	var reply string
	var args interface{}
	if err := c.Call("DemoRPC.GetName",&args,&reply);err != nil{
		fmt.Println("c.Call() fail :",err)
		return "",err
	}
	fmt.Println("reply :",reply)
	return reply,nil

}
