package main

import (
	"bytes"
	"testing"
	"github.com/valyala/bytebufferpool"
)
type ByteBuffer bytebufferpool.ByteBuffer

func (b *ByteBuffer) Write(p []byte) (int, error) {
	return bb(b).Write(p)
}

func (b *ByteBuffer) WriteString(s string) (int, error) {
	return bb(b).WriteString(s)
}

func (b *ByteBuffer) Set(p []byte) {
	bb(b).Set(p)
}

func (b *ByteBuffer) SetString(s string) {
	bb(b).SetString(s)
}

func (b *ByteBuffer) Reset() {
	bb(b).Reset()
}


func AcquireByteBuffer() *ByteBuffer {
	return (*ByteBuffer)(defaultByteBufferPool.Get())
}


func ReleaseByteBuffer(b *ByteBuffer) {
	defaultByteBufferPool.Put(bb(b))
}

func bb(b *ByteBuffer) *bytebufferpool.ByteBuffer {
	return (*bytebufferpool.ByteBuffer)(b)
}

var defaultByteBufferPool bytebufferpool.Pool
func BenchmarkByteBufferWrite(b *testing.B) {
	s := []byte("foobarbaz")
	b.RunParallel(func(pb *testing.PB) {
		var buf ByteBuffer
		for pb.Next() {
			for i := 0; i < 100; i++ {
				buf.Write(s)
			}
			buf.Reset()
		}
	})
}

func BenchmarkBytesBufferWrite(b *testing.B) {
	s := []byte("foobarbaz")
	b.RunParallel(func(pb *testing.PB) {
		var buf bytes.Buffer
		for pb.Next() {
			for i := 0; i < 100; i++ {
				buf.Write(s)
			}
			buf.Reset()
		}
	})
}
