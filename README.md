# yipush  
yipush是一款由golang开发的推送系统，作者练习golang时写的项目，用于移动端或桌面端的推送服务，就目前作者使用相对稳定，简单业务测试可行，目前只支持tcp长连接的连接方式，未来会对websocket的支持  

#协议格式
----------------------------  
  Lenght        |    int32  
  Ver           |    int8  
  Cmd           |    int8  
  HeaderLenght  |    int32  
  Header        |    []byte  
  Data          |    []byte  
  
-----------------------------    
### 学生时期练手项目，不推荐使用。